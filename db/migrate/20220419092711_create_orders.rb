class CreateOrders < ActiveRecord::Migration[5.1]
  def change
    create_table :orders do |t|
      t.string :name
      t.integer :price
      t.bigint :ahoy_visit_id
      t.timestamps
    end
  end
end
