class CreateAhoyMessages < ActiveRecord::Migration[5.1]
  def change
    create_table :ahoy_messages do |t|
      t.references :user, polymorphic: true
      t.text :to
      t.string :mailer
      t.text :subject
      t.string :token
      t.timestamp :opened_at
      t.timestamp :clicked_at
      t.datetime :sent_at
    end
  end
end
