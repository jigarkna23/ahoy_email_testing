AhoyEmail.api = true

AhoyEmail.default_options[:message] = true
AhoyEmail.default_options[:open] = true
AhoyEmail.default_options[:click] = true

class EmailSubscriber
  def open(event)
    event[:controller].ahoy.track "Email opened"
  end

  def click(event)
    event[:controller].ahoy.track "Email clicked"
  end
end

AhoyEmail.subscribers << EmailSubscriber.new